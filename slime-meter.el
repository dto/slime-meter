;;; slime-meter.el --- gathering personal data on SLIME usage  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  David O'Toole

;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Keywords: data, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; The function `sm-start-logging' uses Emacs Lisp advice and hooks to
;; log certain SLIME development related commands with a timestamp,
;; saving them to a file in GNUplot-compatible format.

;; Customize `sm-file' to choose the file name and location. When the
;; data in memory exceed `sm-data-max-length' they will be appended to
;; the file. Use `sm-write' to flush any current data without
;; waiting. If logging is enabled, `sm-write' will be called before
;; exiting Emacs, and also on `auto-save-hook'. To end logging, use
;; `sm-stop-logging'.

;; Currently this package tracks `slime-compile-defun',
;; `slime-compile-and-load-file', `slime-repl-return',
;; `slime-toggle-fancy-trace', `slime-inspect', `sldb-invoke-restart',
;; and `org-archive-subtree', the last of which I use to track
;; completion of a programming TODO list item in Org mode. Please note
;; that changing the status of a TODO item to DONE will not be
;; tracked; you must archive the completed item to trigger tracking.

;; If you have the `slime-asdf' package loaded in your slime-contribs,
;; this package will also track `slime-load-system'.

;; A future version of this program will track more commands,
;; automatically output to GNUplot, and perhaps even compute some
;; statistics. For current GNUplot progress, see the included file
;; `plotting.org'.

;;; Code:

(require 'cl-lib)
(eval-when-compile (require 'cl))

(defgroup slime-meter nil
  "Track SLIME related commands for GNUplot and other analysis."
  :group 'programming)

(defvar sm-data nil
  "List of slime metering data points stored by `sm-log', each in
the form (TIMESTAMP COMMAND).")

(defcustom sm-data-max-length 10
  "Write slime metering data to disk when length exceeds this value."
  :group 'slime-meter
  :type 'integer)

(defun sm-initialize ()
  "Clear the slime-metering data."
  (interactive)
  (setf sm-data nil))

(defcustom sm-file (expand-file-name "~/.slime-meter.dat")
  "File where slime metering data will be appended, in GNUplot
compatible format."
  :group 'slime-meter
  :type 'file)

(defun sm-write ()
  "Append the current slime metering data to `sm-file'."
  (interactive)
  (with-temp-buffer
    (cl-dolist (entry (reverse sm-data))
      (insert (format "%S %S\n" (first entry) (second entry))))
    (unless (null sm-data)
      (append-to-file (point-min) (point-max) sm-file)
      (setf sm-data nil))))

(defun sm-write-maybe ()
  "Hook function to write slime metering data when needed.
See also `sm-data-max-length'."
  (when (> (length sm-data) sm-data-max-length)
    (sm-write)))

(defcustom sm-commands '(org-archive-subtree
                         slime
                         slime-compile-defun
                         slime-compile-and-load-file
                         slime-repl-return
                         slime-toggle-fancy-trace
                         slime-inspect
                         sldb-invoke-restart
                         slime-load-system)
  "List of commands to be tracked. This list orders the commands
and allows them to be assigned an integer for GNUplot."
  :group 'slime-meter
  :type 'list)

(defun sm-command-number (command)
  "Return the integer associated with COMMAND. See also
`sm-commands'."
  (position command sm-commands))

(defun sm-log (command)
  "Add a timestamped entry to the slime metering data with the
current COMMAND."
  (push (list
            (time-convert (current-time) 'integer)
            (sm-command-number command))
        sm-data)
  (sm-write-maybe))
  
(defun sm-start-logging ()
  "Install hooks and advice to log commands for slime metering."
  (interactive)
  (message "Installing slime metering hooks...")
  (sm-initialize)
  (add-hook 'kill-emacs-hook 'sm-write)
  (add-hook 'auto-save-hook 'sm-write)
  (defadvice slime (after slime-meter activate)
    (sm-log 'slime))
  (defadvice slime-inspect (after slime-meter activate)
    (sm-log 'slime-inspect))
  (defadvice slime-toggle-fancy-trace (after slime-meter activate)
    (sm-log 'slime-toggle-fancy-trace))
  (defadvice slime-repl-return (after slime-meter activate)
    (sm-log 'slime-repl-return))
  (defadvice slime-compile-defun (after slime-meter activate)
    (sm-log 'slime-compile-defun))
  (defadvice slime-compile-and-load-file (after slime-meter activate)
    (sm-log 'slime-compile-and-load-file))
  (defadvice org-archive-subtree (after slime-meter activate)
    (sm-log 'org-archive-subtree))
  (defadvice sldb-invoke-restart (after slime-meter activate)
    (sm-log 'sldb-invoke-restart))
  (when (fboundp 'slime-load-system)
    (defadvice slime-load-system (after slime-meter activate)
      (sm-log 'slime-load-system)))
  (message "Installing slime metering hooks... Done."))

(defun sm-stop-logging ()
  "Uninstall hooks and advice to stop logging."
  (interactive)
  (message "Removing slime metering hooks...")
  (sm-write)
  (remove-hook 'kill-emacs-hook 'sm-write)
  (remove-hook 'auto-save-hook 'sm-write)
  (advice-remove 'slime 'slime-meter)
  (advice-remove 'slime-inspect 'slime-meter)
  (advice-remove 'slime-toggle-fancy-trace 'slime-meter)
  (advice-remove 'slime-compile-defun 'slime-meter)
  (advice-remove 'slime-repl-return 'slime-meter)
  (advice-remove 'slime-compile-and-load-file 'slime-meter)
  (advice-remove 'org-archive-subtree 'slime-meter)
  (advice-remove 'sldb-invoke-restart 'slime-meter)
  (when (fboundp 'slime-load-system)
    (advice-remove 'slime-load-system 'slime-meter))
  (message "Removing slime metering hooks... Done."))

(defun sm-read-data (file)
  (with-temp-buffer
    (insert-file-contents file)
    (goto-char (point-min))
    (insert "(")
    (goto-char (point-max))
    (insert ")")
    (car (read-from-string (buffer-substring-no-properties (point-min) (point-max))))))

(defun sm-split-data (data)
  (let (result)
    (while data
      (push (list (pop data) (pop data))
            result))
    (nreverse result)))

(defun sm-analyze ()
  (interactive)
  (sm-write)
  (let* ((data (sm-split-data (sm-read-data sm-file)))
         (n (length sm-commands))
         (counts (make-vector n 0)))
    (cl-dolist (d data)
      (cl-destructuring-bind (timestamp command-number) d
        (cl-incf (aref counts command-number))))
    (switch-to-buffer (get-buffer-create "*slime-meter-analysis*"))
    (delete-region (point-min) (point-max))
    (let ((org-archive-subtree (aref counts 0))
          (slime (aref counts 1))
          (slime-compile-defun (aref counts 2))
          (slime-compile-and-load-file (aref counts 3))
          (slime-repl-return (aref counts 4))
          (slime-toggle-fancy-trace (aref counts 5))
          (slime-inspect (aref counts 6))
          (sldb-invoke-restart (aref counts 7))
          (slime-load-system (aref counts 8)))
      (cl-macrolet ((pr (sym)
                     `(insert (format "%S: %S\n"
                                      ',sym
                                      ,sym))))
        (pr org-archive-subtree)
        (pr slime)
        (pr slime-compile-defun)
        (pr slime-compile-and-load-file)
        (pr slime-repl-return)
        (pr slime-toggle-fancy-trace)
        (pr slime-inspect)
        (pr sldb-invoke-restart)
        (pr slime-load-system)
        (when (not (zerop org-archive-subtree))
          (let ((slime-compile-defun/todos (/ (float slime-compile-defun) (float org-archive-subtree)))
                (slime/todos (/ (float slime) (float org-archive-subtree)))
                (slime-compile-and-load-file/todos (/ (float slime-compile-and-load-file) (float org-archive-subtree)))
                (slime-repl-return/todos (/ (float slime-repl-return) (float org-archive-subtree)))
                (slime-toggle-fancy-trace/todos (/ (float slime-toggle-fancy-trace) (float org-archive-subtree)))
                (slime-inspect/todos (/ (float slime-inspect) (float org-archive-subtree)))
                (slime-load-system/todos (/ (float slime-load-system) (float org-archive-subtree)))
                (sldb-invoke-restart/todos (/ (float sldb-invoke-restart) (float org-archive-subtree))))
            (let ((all-other-commands/todos (/ (float (+
                                                       slime
                                                       slime-compile-defun
                                                       slime-compile-and-load-file
                                                       slime-repl-return
                                                       slime-toggle-fancy-trace
                                                       slime-inspect
                                                       slime-load-system
                                                       sldb-invoke-restart
                                                       ))
                                   (float org-archive-subtree))))
              (pr slime-compile-defun/todos)
              (pr slime/todos)
              (pr slime-repl-return/todos)
              (pr slime-compile-and-load-file/todos)
              (pr slime-toggle-fancy-trace/todos)
              (pr slime-inspect/todos)
              (pr sldb-invoke-restart/todos)
              (pr slime-load-system/todos)
              (pr all-other-commands/todos))))))))
                                      
(provide 'slime-meter)
;;; slime-meter.el ends here
